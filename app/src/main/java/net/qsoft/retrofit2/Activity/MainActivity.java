package net.qsoft.retrofit2.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import net.qsoft.retrofit2.Adapter.RecyclerAdapter;
import net.qsoft.retrofit2.Model.DataModel;
import net.qsoft.retrofit2.Network.ApiClient;
import net.qsoft.retrofit2.R;
import net.qsoft.retrofit2.Service.APIService;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private LinearLayoutManager layoutManager;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    RecyclerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
       // getUserList();
    }

    private void getUserList() {
        try {
            APIService service = ApiClient.getRetrofit().create(APIService.class);
            Call<List<DataModel>> call = service.getData();

            call.enqueue(new Callback<List<DataModel>>() {
                @Override
                public void onResponse(Call<List<DataModel>> call, Response<List<DataModel>> response) {
                    Log.d("onResponse", response.message());

                    List<DataModel> itemList = response.body();

                    ButterKnife.bind(MainActivity.this);

                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());

                    recyclerView.setLayoutManager(mLayoutManager);

                    adapter = new RecyclerAdapter(itemList);

                    recyclerView.setAdapter(adapter);

                }

                @Override
                public void onFailure(Call<List<DataModel>> call, Throwable t) {

                    Toast.makeText(getApplicationContext(), "Fail to connetct server!", Toast.LENGTH_LONG).show();
                }
            });
        } catch (Exception e) {
        }
    }
}