package net.qsoft.retrofit2.Activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import net.qsoft.retrofit2.Adapter.ProductAdapter;
import net.qsoft.retrofit2.Adapter.TestAdapter;
import net.qsoft.retrofit2.Model.Product;
import net.qsoft.retrofit2.Model.TestModel;
import net.qsoft.retrofit2.Network.ApiClient;
import net.qsoft.retrofit2.R;
import net.qsoft.retrofit2.Service.APIService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by QSPA10 on 3/5/2018.
 */

public class ProductListActivity extends AppCompatActivity {

    private LinearLayoutManager layoutManager;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    TestAdapter adapter;
    ProductAdapter adapter1;

    private ArrayList<Product> data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getProductList2();
    }

    private void getProductList2() {
        try {
            APIService service = ApiClient.getRetrofit().create(APIService.class);

            Call<TestModel> call = service.getProduct();

            call.enqueue(new Callback<TestModel>() {
                @Override
                public void onResponse(Call<TestModel> call, Response<TestModel> response) {

                    Log.d("onResponse", response.message());

                    TestModel testModel = response.body();

                    ButterKnife.bind(ProductListActivity.this);

                    data = new ArrayList<>(Arrays.asList(testModel.getData()));

                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());

                    recyclerView.setLayoutManager(mLayoutManager);

                    adapter = new TestAdapter(data);

                    recyclerView.setAdapter(adapter);
                }

                @Override
                public void onFailure(Call<TestModel> call, Throwable t) {

                    Toast.makeText(getApplicationContext(), "Fail to connetct server!", Toast.LENGTH_LONG).show();
                }
            });
        } catch (Exception e) {
        }
    }

    private void getProductList1() {
        try {
            APIService service = ApiClient.getRetrofit().create(APIService.class);
            Call<List<Product>> call = service.getProductData();

            call.enqueue(new Callback<List<Product>>() {
                @Override
                public void onResponse(Call<List<Product>> call, Response<List<Product>> response) {
                    Log.d("onResponse", response.message());

                    List<Product> productList = response.body();

                    ButterKnife.bind(ProductListActivity.this);

                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());

                    recyclerView.setLayoutManager(mLayoutManager);

                    adapter1 = new ProductAdapter(productList);

                    recyclerView.setAdapter(adapter);
                }

                @Override
                public void onFailure(Call<List<Product>> call, Throwable t) {

                    Toast.makeText(getApplicationContext(), "Fail to connetct server!", Toast.LENGTH_LONG).show();
                }

            });
        } catch (Exception e) {
        }
    }
}