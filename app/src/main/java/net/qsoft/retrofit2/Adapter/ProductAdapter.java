package net.qsoft.retrofit2.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import net.qsoft.retrofit2.Model.DataModel;
import net.qsoft.retrofit2.Model.Product;
import net.qsoft.retrofit2.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by QSPA10 on 3/5/2018.
 */

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.MyViewHolder> {

    private List<Product> productList;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.pid)
        TextView pid;

        @BindView(R.id.name)
        TextView name;

        @BindView(R.id.price)
        TextView price;

        @BindView(R.id.description)
        TextView description;

        @BindView(R.id.created_at)
        TextView created_at;

        @BindView(R.id.updated_at)
        TextView updated_at;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }


    public ProductAdapter(List<Product> productList) {
        this.productList = productList;
    }

    @Override
    public ProductAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_product, parent, false);

        return new ProductAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ProductAdapter.MyViewHolder holder, int position) {

        Product product = productList.get(position);

        holder.pid.setText(product.getPid());
        holder.name.setText(product.getName());
        holder.price.setText(product.getPrice());
        holder.description.setText(product.getDescription());
        holder.created_at.setText(product.getCreated_at());
        holder.updated_at.setText(product.getUpdated_at());
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

}
