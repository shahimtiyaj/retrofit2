package net.qsoft.retrofit2.Service;

import net.qsoft.retrofit2.Model.DataModel;
import net.qsoft.retrofit2.Model.Product;
import net.qsoft.retrofit2.Model.TestModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by QSPA10 on 2/17/2018.
 */

public interface APIService {
    @GET("branch_information")
    Call<List<DataModel>> getData();

    @GET("Product/product_list.php")
    Call<List<Product>> getProductData();

    @GET("Product/get_product_details.php")
    Call<TestModel> getProduct();
}
