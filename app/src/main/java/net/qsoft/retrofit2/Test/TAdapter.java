package net.qsoft.retrofit2.Test;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import net.qsoft.retrofit2.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by QSPA10 on 3/7/2018.
 */

public class TAdapter extends RecyclerView.Adapter<TAdapter.MyViewHolder> {

    private ArrayList<TModel> data;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.pid)
        TextView pid;

        @BindView(R.id.name)
        TextView name;

        @BindView(R.id.price)
        TextView price;

        @BindView(R.id.description)
        TextView description;

        @BindView(R.id.created_at)
        TextView created_at;

        @BindView(R.id.updated_at)
        TextView updated_at;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }


    public TAdapter(ArrayList<TModel> data) {
        this.data = data;
    }

    @Override
    public TAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_product, parent, false);

        return new TAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(TAdapter.MyViewHolder holder, int position) {

        holder.pid.setText(data.get(position).getPid());
        holder.name.setText(data.get(position).getName());
        holder.price.setText(data.get(position).getPrice());
        holder.description.setText(data.get(position).getDescription());
        holder.created_at.setText(data.get(position).getCreated_at());
        holder.updated_at.setText(data.get(position).getUpdated_at());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

}