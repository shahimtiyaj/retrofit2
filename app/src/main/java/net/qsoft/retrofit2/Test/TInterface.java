package net.qsoft.retrofit2.Test;

/**
 * Created by QSPA10 on 3/7/2018.
 */

import retrofit2.Call;
import retrofit2.http.GET;

public interface TInterface {
    @GET("Product/product_list.php")
    Call<JSONResponse> getProductItem();
}
