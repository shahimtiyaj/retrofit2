package net.qsoft.retrofit2.Test;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import net.qsoft.retrofit2.Activity.ProductListActivity;
import net.qsoft.retrofit2.Adapter.TestAdapter;
import net.qsoft.retrofit2.Model.TestModel;
import net.qsoft.retrofit2.Network.ApiClient;
import net.qsoft.retrofit2.R;
import net.qsoft.retrofit2.Service.APIService;

import java.util.ArrayList;
import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by QSPA10 on 3/7/2018.
 */

public class TListActivity extends AppCompatActivity {

    private LinearLayoutManager layoutManager;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    TAdapter adapter;
    private ArrayList<TModel> data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getProductListItem();
    }

    private void getProductListItem() {
        try {
            TInterface service = ApiClient.getRetrofit().create(TInterface.class);

            Call<JSONResponse> call = service.getProductItem();

            call.enqueue(new Callback<JSONResponse>() {
                @Override
                public void onResponse(Call<JSONResponse> call, Response<JSONResponse> response) {

                    Log.d("onResponse", response.message());

                    JSONResponse jsonResponse = response.body();

                    ButterKnife.bind(TListActivity.this);

                    data = new ArrayList<>(Arrays.asList(jsonResponse.getData()));

                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());

                    recyclerView.setLayoutManager(mLayoutManager);

                    adapter = new TAdapter(data);

                    recyclerView.setAdapter(adapter);
                }

                @Override
                public void onFailure(Call<JSONResponse> call, Throwable t) {

                    Toast.makeText(getApplicationContext(), "Fail to connetct server!", Toast.LENGTH_LONG).show();
                }
            });
        } catch (Exception e) {
        }
    }
}
