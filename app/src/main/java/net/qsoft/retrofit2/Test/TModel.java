package net.qsoft.retrofit2.Test;

/**
 * Created by QSPA10 on 3/7/2018.
 */

public class TModel {
    String pid;
    String name;
    String price;
    String description;
    String created_at;
    String updated_at;

    public String getPid() {
        return pid;
    }

    public TModel(String pid, String name, String price, String description, String created_at, String updated_at) {
        this.pid = pid;
        this.name = name;
        this.price = price;
        this.description = description;
        this.created_at = created_at;
        this.updated_at = updated_at;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

}
